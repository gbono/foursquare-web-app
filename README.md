# FourSquare web app  (Angular 6) - Giampiero Bono

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) latest version.

## Project setup + start

Project generated with:
`node v10.0.0`
`yarn v1.6.0`

1. `git clone git@gitlab.com:gbono/foursquare-web-app.git`
2. `cd foursquare-web-app`
3. `yarn install`
3. `yarn start`

The application will be built using webpack and it can be reached via browser thanks to a dev server. 

## Architectural decisions

- Project has been generated from scratch with Angular CLI `@latest`. This tool allowed a dev speed up.
- UI components by [angular-material](https://material.angular.io/)
- Application UI components react to application states changes.
This pattern comes out from React team but it has been ported to Angular thanks to[ngrx library](https://github.com/ngrx/platform).
- The interaction between the UI and the store is done using specific Angular services, acting as a kind of "data access layer". 
This is done to allow a complete decouple and a better organized architecture.
- UI Components are split by container / presenter structure as recommended by the React team
- Used some pure function helpers to manipulate data and perform operations without any side effect.
- Used interfaces instead of classes to compose instead of inherit the business object model.

## Functional view

The UI interface is kept simple and the colors chosen come directly from the FourSquare website. 

The interface is composed by:
- The home page allowing user to select a location (by default the user will be geo localized) and, if needed, a category.
- A search result page containing the list of found venues and a map to display their position. Clicking on one venue detail, 
the map will be automatically zoomed on the venue map pushpin. 

## Style points

- The base I am using is Bootstrap 4 for grid / flex utilities and Angular Material for components.
- Some mixins created to generate resusable rules.
- Trying to follow a mobile first approach
- Designed to have a view ready for mobile, tablet and desktop. 

## Tests

- Services, ngrx reducers and effects fully tested.

## Missing points

- Test for UI components. Because of this I cannot ensure all the scenarios are working properly. 
- APIs secrets hardcoded. That's pretty bad I know. The initial idea was to create a kind of proxy in node to hide the secrets or a node 
server to perform the ooauth and return the auth token, but this solution would require much more codiing time.

# Main npm tasks

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
The same could be done with `ng serve --aot` to perform an AOT compilation

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Continuous integration and deployment

In order to verify each commit, GitLab pipelines have been setup in order to build the webapp
after each pull request (since I was alone I was using directly master, so actually I should say: after each push).
If the build is successfully, the webapp will be automatically deployed on Surge. 

## APIs used

FourSquare and Google GeoCode APIs are used for this webapp. The first one, to retrieve list, categories and
details about venues, the second one to geo localize the user exploiting the "navigator" object if
available on the browser used. 
