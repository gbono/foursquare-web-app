import { getTestBed, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import {
  FailVenueDetails,
  LoadVenueDetails, SelectVenueDetails,
  UpdateVenueDetails,
} from '../../actions/venue-details/venue-details.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IVenueDetailsResponse } from '../../models/foursquare/venue-details';
import { fakeVenueDetails } from '../../services/data-provider/mock/fake-responses/fake-venue-details-response';
import { initialState, reducer } from './venue-details.reducer';

describe('VenueDetails reducer', () => {
  beforeAll(() => getTestBed().platform || TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()));
  describe('reducers', () => {

    const fakeError = {
      message: 'oops..i did it again',
    };

    it('should return the initial status', () => {
      const state = reducer(undefined, {type: 'foo'} as any);
      expect(state).toEqual(initialState);
      expect(state.stateStatus).toBe(StateStatus.ready);
    });

    describe('on LOAD action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initialState, new LoadVenueDetails(Promise.resolve<IVenueDetailsResponse>(fakeVenueDetails as any)));
        expect(state.stateStatus).toEqual(StateStatus.loading);
        done();
      });
    });

    describe('on UPDATE action', () => {
      it('should change the status and set a state content', () => {
        const state = reducer(initialState, new UpdateVenueDetails(fakeVenueDetails.response as any));
        expect(state.stateStatus).toEqual(StateStatus.ready);
        expect(state.entities).toBeDefined();
      });
    });

    describe('on FAIL action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initialState, new FailVenueDetails(fakeError));
        expect(state.stateStatus).toEqual(StateStatus.failed);
        done();
      });
    });

    describe('on SELECT action', () => {
      it('should change the selected venue details id', () => {
        const state = reducer(initialState, new SelectVenueDetails('test_ID'));
        expect(state.selectedVenueDetails).toBe('test_ID');
      });
    });
  });
});
