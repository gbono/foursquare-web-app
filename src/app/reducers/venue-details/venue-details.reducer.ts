import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { VenueDetailsActions, VenueDetailsActionsTypes } from '../../actions/venue-details/venue-details.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IAsyncState } from '../../common/state/async-state';
import { IVenueDetails } from '../../models';

export interface IState extends EntityState<IVenueDetails>, IAsyncState {
  selectedVenueDetails: string | null;
}

export const adapter: EntityAdapter<IVenueDetails> = createEntityAdapter<IVenueDetails>({
  selectId: (venueDetails: IVenueDetails) => venueDetails.id,
  sortComparer: false,
});

export const initialState: IState = adapter.getInitialState({
  selectedVenueDetails: null,
  stateStatus: StateStatus.ready,
});

export function reducer(state: IState = initialState, action: VenueDetailsActions) {
  switch (action.type) {
    case VenueDetailsActionsTypes.Load: {
      return {
        ...state,
        stateStatus: StateStatus.loading,
      };
    }

    case VenueDetailsActionsTypes.Update: {
      return adapter.addOne(action.payload, {
        ...state,
        stateStatus: StateStatus.ready,
      });
    }

    case VenueDetailsActionsTypes.Fail: {
      return {
        ...state,
        stateStatus: StateStatus.failed,
      };
    }

    case VenueDetailsActionsTypes.Select: {
      return {
        ...state,
        selectedVenueDetails: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}
