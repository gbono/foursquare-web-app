import * as fromRouter from '@ngrx/router-store';
import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';

import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';
import { IRouterStateUrl } from '../common/custom-router-serializer';

import * as fromGeoCode from './geo-code/geo-code.reducer';
import * as fromUserSearchCriteria from './user-search-criteria/user-search-criteria.reducer';
import * as fromVenueDetails from './venue-details/venue-details.reducer';
import * as fromVenuesCategories from './venues-categories/venues-categories.reducer';
import * as fromVenuesSearch from './venues-search/venues-search.reducer';

export interface IState {
  router: fromRouter.RouterReducerState<IRouterStateUrl>;
  venuesCategories: fromVenuesCategories.IState;
  geoCode: fromGeoCode.IState;
  venuesSearch: fromVenuesSearch.IState;
  userSearchCriteria: fromUserSearchCriteria.IState;
  venueDetails: fromVenueDetails.IState;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<IState> = {
  router: fromRouter.routerReducer,
  venuesCategories: fromVenuesCategories.reducer,
  geoCode: fromGeoCode.reducer,
  venuesSearch: fromVenuesSearch.reducer,
  userSearchCriteria: fromUserSearchCriteria.reducer,
  venueDetails: fromVenueDetails.reducer,
};

// console.log all actions
export function logger(reducer: ActionReducer<IState>): ActionReducer<IState> {
  return (state: IState, action: any): IState => {
    console.log('state', state);
    console.log('action', action);
    return reducer(state, action);
  };
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: Array<MetaReducer<IState>> = !environment.production
  ? [logger, storeFreeze]
  : [];
