import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { GeoCodeActions, GeoCodeActionsTypes } from '../../actions/geo-code/geo-code.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IAsyncState } from '../../common/state/async-state';
import { IGeoCode } from '../../models';

export interface IState extends EntityState<IGeoCode>, IAsyncState {
}

export const adapter: EntityAdapter<IGeoCode> = createEntityAdapter<IGeoCode>({
  selectId: (geoCode: IGeoCode) => geoCode.place_id,
  sortComparer: false,
});

export const initialState: IState = adapter.getInitialState({
  stateStatus: StateStatus.ready,
});

export function reducer(state: IState = initialState, action: GeoCodeActions) {
  switch (action.type) {
    case GeoCodeActionsTypes.Load: {
      return {
        ...initialState,
        stateStatus: StateStatus.loading,
      };
    }

    case GeoCodeActionsTypes.Update: {
      return adapter.addMany(action.payload, {
        ...state,
        stateStatus: StateStatus.ready,
      });
    }

    case GeoCodeActionsTypes.Fail: {
      return {
        ...state,
        stateStatus: StateStatus.failed,
      };
    }

    default: {
      return state;
    }
  }
}
