import { getTestBed, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { FailGeoCode, LoadGeoCode, UpdateGeoCode } from '../../actions/geo-code/geo-code.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IGeoCodeResponse } from '../../models/geo-code';
import { fakeGeoCodeResponse } from '../../services/data-provider/mock/fake-responses/fake-geo-code-responses';
import { initialState, reducer } from './geo-code.reducer';

describe('GeoCode reducer', () => {
  beforeAll(() => getTestBed().platform || TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()));
  describe('reducers', () => {

    const fakeError = {
      message: 'oops..i did it again',
    };

    it('should return the initial status', () => {
      const state = reducer(undefined, {type: 'foo'} as any);
      expect(state).toEqual(initialState);
      expect(state.stateStatus).toBe(StateStatus.ready);
    });

    describe('on LOAD action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initialState, new LoadGeoCode(Promise.resolve<IGeoCodeResponse>(fakeGeoCodeResponse as any)));
        expect(state.stateStatus).toEqual(StateStatus.loading);
        done();
      });
    });

    describe('on UPDATE action', () => {
      it('should change the status and set a state content', () => {
        const state = reducer(initialState, new UpdateGeoCode(fakeGeoCodeResponse.results as any));
        expect(state.stateStatus).toEqual(StateStatus.ready);
        expect(state.entities).toBeDefined();
      });
    });

    describe('on FAIL action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initialState, new FailGeoCode(fakeError));
        expect(state.stateStatus).toEqual(StateStatus.failed);
        done();
      });
    });
  });
});
