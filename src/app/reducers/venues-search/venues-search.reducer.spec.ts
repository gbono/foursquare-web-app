import { getTestBed, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import {
  FailVenuesSearch,
  LoadVenuesSearch,
  UpdateVenuesSearch,
} from '../../actions/venues-serach/venues-search.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IVenueSearchResponse } from '../../models/foursquare/venues-search';
import { fakeVenuesSearchResponse } from '../../services/data-provider/mock/fake-responses/fake-venues-search-response';
import { initialState, reducer } from './venues-search.reducer';

describe('VenuesSearch reducer', () => {
  beforeAll(() => getTestBed().platform || TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()));
  describe('reducers', () => {

    const fakeError = {
      message: 'oops..i did it again',
    };

    it('should return the initial status', () => {
      const state = reducer(undefined, {type: 'foo'} as any);
      expect(state).toEqual(initialState);
      expect(state.stateStatus).toBe(StateStatus.ready);
    });

    describe('on LOAD action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initialState,
          new LoadVenuesSearch(Promise.resolve<IVenueSearchResponse>(fakeVenuesSearchResponse as any)));
        expect(state.stateStatus).toEqual(StateStatus.loading);
        done();
      });
    });

    describe('on UPDATE action', () => {
      it('should change the status and set a state content', () => {
        const state = reducer(initialState, new UpdateVenuesSearch(fakeVenuesSearchResponse.response.venues as any));
        expect(state.stateStatus).toEqual(StateStatus.ready);
        expect(state.entities).toBeDefined();
      });
    });

    describe('on FAIL action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initialState, new FailVenuesSearch(fakeError));
        expect(state.stateStatus).toEqual(StateStatus.failed);
        done();
      });
    });
  });
});
