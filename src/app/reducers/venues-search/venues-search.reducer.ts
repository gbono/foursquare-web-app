import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { VenuesSearchActions, VenuesSearchActionsTypes } from '../../actions/venues-serach/venues-search.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IAsyncState } from '../../common/state/async-state';
import { IVenue } from '../../models/foursquare';

export interface IState extends EntityState<IVenue>, IAsyncState {
}

export const adapter: EntityAdapter<IVenue> = createEntityAdapter<IVenue>({
  selectId: (venue: IVenue) => venue.id,
  sortComparer: false,
});

export const initialState: IState = adapter.getInitialState({
  stateStatus: StateStatus.ready,
});

export function reducer(state: IState = initialState, action: VenuesSearchActions) {
  switch (action.type) {
    case VenuesSearchActionsTypes.Load: {
      return {
        ...initialState,
        stateStatus: StateStatus.loading,
      };
    }

    case VenuesSearchActionsTypes.Update: {
      return adapter.addMany(action.payload, {
        ...state,
        stateStatus: StateStatus.ready,
      });
    }

    case VenuesSearchActionsTypes.Fail: {
      return {
        ...state,
        stateStatus: StateStatus.failed,
      };
    }

    default: {
      return state;
    }
  }
}
