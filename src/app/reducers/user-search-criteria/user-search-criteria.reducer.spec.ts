import { getTestBed, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import {
  ClearSearchCriteria,
  NewSearchCriteria,
} from '../../actions/user-search-criteria/user-search-criteria.actions';
import { initialState, reducer } from './user-search-criteria.reducer';

describe('UserSearchCriteria reducer', () => {
  beforeAll(() => getTestBed().platform || TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()));
  describe('reducers', () => {

    const fakeError = {
      message: 'oops..i did it again',
    };

    it('should return the initial status', () => {
      const state = reducer(undefined, {type: 'foo'} as any);
      expect(state).toEqual(initialState);
    });

    describe('on NewSearchCriteria action', () => {
      it('should set the new search criteria', () => {
        const state = reducer(initialState, new NewSearchCriteria({categoryId: 'test', location: 'test'}));
        expect(state.content).toEqual({categoryId: 'test', location: 'test'});
      });
    });

    describe('on ClearSearchCriteria action', () => {
      it('should clear the search criteria', () => {
        const state = reducer(initialState, new ClearSearchCriteria());
        expect(state).toEqual(initialState);
      });
    });
  });
});
