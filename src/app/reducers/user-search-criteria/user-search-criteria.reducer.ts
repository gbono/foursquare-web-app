import {
  UserSearchCriteriaActions,
  UserSearchCriteriaActionsTypes,
} from '../../actions/user-search-criteria/user-search-criteria.actions';
import { IUserSearchCriteria } from '../../models';

export interface IState {
  content: IUserSearchCriteria;
}

export const initialState: IState = {
  content: {
    categoryId: '',
    location: '',
  },
};

export function reducer(state: IState = initialState, action: UserSearchCriteriaActions) {
  switch (action.type) {
    case UserSearchCriteriaActionsTypes.NewSearchCriteria: {
      return {
        content: {...action.payload},
      };
    }

    case UserSearchCriteriaActionsTypes.ClearSearchCriteria: {
      return {
        ...initialState,
      };
    }

    default: {
      return state;
    }
  }
}
