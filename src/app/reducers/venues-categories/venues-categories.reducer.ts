import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import {
  VenuesCategoriesActions,
  VenuesCategoriesActionsTypes,
} from '../../actions/venues-categories/venues-categories.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IAsyncState } from '../../common/state/async-state';
import { IVenueCategory } from '../../models/foursquare';

export interface IState extends EntityState<IVenueCategory>, IAsyncState {
}

export const adapter: EntityAdapter<IVenueCategory> = createEntityAdapter<IVenueCategory>({
  selectId: (venueCategory: IVenueCategory) => venueCategory.id,
  sortComparer: false,
});

export const initialState: IState = adapter.getInitialState({
  stateStatus: StateStatus.ready,
});

export function reducer(state: IState = initialState, action: VenuesCategoriesActions) {
  switch (action.type) {
    case VenuesCategoriesActionsTypes.Load: {
      return {
        ...initialState,
        stateStatus: StateStatus.loading,
      };
    }

    case VenuesCategoriesActionsTypes.Update: {
      return adapter.addMany(action.payload, {
        ...state,
        stateStatus: StateStatus.ready,
      });
    }

    case VenuesCategoriesActionsTypes.Fail: {
      return {
        ...state,
        stateStatus: StateStatus.failed,
      };
    }

    default: {
      return state;
    }
  }
}
