import { getTestBed, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import {
  FailVenuesCategories,
  LoadVenuesCategories,
  UpdateVenuesCategories,
} from '../../actions/venues-categories/venues-categories.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IVenueCategoriesResponse } from '../../models/foursquare/venue-categories';
import { fakeVenuesCategories } from '../../services/data-provider/mock/fake-responses/fake-venues-categories';
import { initialState, reducer } from './venues-categories.reducer';

describe('VenuesCategories reducer', () => {
  beforeAll(() => getTestBed().platform || TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()));
  describe('reducers', () => {

    const fakeError = {
      message: 'oops..i did it again',
    };

    it('should return the initial status', () => {
      const state = reducer(undefined, {type: 'foo'} as any);
      expect(state).toEqual(initialState);
      expect(state.stateStatus).toBe(StateStatus.ready);
    });

    describe('on LOAD action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initialState,
          new LoadVenuesCategories(Promise.resolve<IVenueCategoriesResponse>(fakeVenuesCategories as any)));
        expect(state.stateStatus).toEqual(StateStatus.loading);
        done();
      });
    });

    describe('on UPDATE action', () => {
      it('should change the status and set a state content', () => {
        const state = reducer(initialState, new UpdateVenuesCategories(fakeVenuesCategories.response.categories as any));
        expect(state.stateStatus).toEqual(StateStatus.ready);
        expect(state.entities).toBeDefined();
      });
    });

    describe('on FAIL action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initialState, new FailVenuesCategories(fakeError));
        expect(state.stateStatus).toEqual(StateStatus.failed);
        done();
      });
    });
  });
});
