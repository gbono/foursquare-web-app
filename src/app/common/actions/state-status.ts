export enum StateStatus {
  ready = 'ready',
  loading = 'loading',
  failed = 'failed',
}
