import { IBaseAction } from './base-action';

export interface ICustomAsyncAction<T> extends IBaseAction {
  asyncPayload: Promise<T>;
}
