import { IBaseAction } from './base-action';

export interface ICustomAction<T> extends IBaseAction {
  payload: T;
}
