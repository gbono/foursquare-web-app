import { Action } from '@ngrx/store';

export interface IBaseAction extends Action {
  errors?: any;
}
