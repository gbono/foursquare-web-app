export const buildGeoCodeUrl = (baseGoogleApiUrl) => (latitude, longitude) =>
  `${baseGoogleApiUrl}json?latlng=${latitude},${longitude}&sensor=true&key=AIzaSyATCWZqTwZUA3iJVeO_OwoCzHDQtf5Sle8`;
