export const buildGetVenuesCategoriesUrl = (baseUrl: string) => `${baseUrl}/categories`;

export const buildFindVenuesByCategoryIdAndLocationUrl = (baseUrl: string) => (categoryId: string, location: string) =>
  `${baseUrl}/search?near=${location}&categoryId=${categoryId}`;

export const buildGetVenueDetailsByIdUrl = (baseUrl: string) => (venueId: string) => `${baseUrl}/${venueId}`;
