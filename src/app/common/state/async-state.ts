import { StateStatus } from '../actions/state-status';

export interface IAsyncState {
  stateStatus: StateStatus;
}
