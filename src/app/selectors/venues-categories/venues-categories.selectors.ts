import { createFeatureSelector, createSelector } from '@ngrx/store';
import { adapter, IState } from '../../reducers/venues-categories/venues-categories.reducer';

export const venuesCategoriesState = createFeatureSelector<IState>('venuesCategories');
export const venuesCategoriesStateStatus =
  createSelector(venuesCategoriesState, (state: IState) => state && state.stateStatus);

export const {
  selectIds: selectVenuesCategoriesIds,
  selectEntities: selectVenuesCategoriesEntities,
  selectAll: selectAllVenuesCategories,
} = adapter.getSelectors(venuesCategoriesState);
