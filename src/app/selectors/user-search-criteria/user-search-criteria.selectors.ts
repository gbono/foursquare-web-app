import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IState } from '../../reducers/user-search-criteria/user-search-criteria.reducer';

export const userSearchCriteriaState = createFeatureSelector<IState>('userSearchCriteria');
export const userSearchCriteria =
  createSelector(userSearchCriteriaState, (state: IState) => state.content);
