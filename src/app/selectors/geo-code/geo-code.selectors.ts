import { createFeatureSelector } from '@ngrx/store';
import { adapter, IState } from '../../reducers/geo-code/geo-code.reducer';

export const geoCodeState = createFeatureSelector<IState>('geoCode');

export const {
  selectIds: selectGeoCodeIds,
  selectEntities: selectGeoCodeEntities,
  selectAll: selectAllGeoCode,
} = adapter.getSelectors(geoCodeState);
