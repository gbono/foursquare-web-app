import { createFeatureSelector, createSelector } from '@ngrx/store';
import { adapter, IState } from '../../reducers/venue-details/venue-details.reducer';

export const venueDetailsState = createFeatureSelector<IState>('venueDetails');
export const venueDetailsStateStatus =
  createSelector(venueDetailsState, (state: IState) => state && state.stateStatus);

export const {
  selectIds: selectVenueDetailsIds,
  selectEntities: selectVenueDetailsEntities,
  selectAll: selectAllVenueDetails,
} = adapter.getSelectors(venueDetailsState);

export const venueDetailsById = (venueId) => createSelector(selectVenueDetailsEntities,
  (venueDetails) => venueDetails && venueDetails[venueId]);

export const getSelectedVenueDetailsId = createSelector(venueDetailsState, (state: IState) => state.selectedVenueDetails);

export const getSelectedVenueDetails = createSelector(selectVenueDetailsEntities, getSelectedVenueDetailsId,
  (entities, selectedId) => selectedId && entities[selectedId]);
