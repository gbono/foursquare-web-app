import { createFeatureSelector, createSelector } from '@ngrx/store';
import { adapter, IState } from '../../reducers/venues-search/venues-search.reducer';

export const venuesSearchState = createFeatureSelector<IState>('venuesSearch');
export const venuesSearchStateStatus =
  createSelector(venuesSearchState, (state: IState) => state && state.stateStatus);

export const {
  selectIds: selectVenuesSearchIds,
  selectEntities: selectVenuesSearchEntities,
  selectAll: selectAllVenuesSearch,
} = adapter.getSelectors(venuesSearchState);
