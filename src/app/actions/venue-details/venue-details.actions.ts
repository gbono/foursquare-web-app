import { IBaseAction } from '../../common/actions/base-action';
import { ICustomAction } from '../../common/actions/custom-action';
import { ICustomAsyncAction } from '../../common/actions/custom-async-action';
import { IVenueDetails } from '../../models';
import { IVenueDetailsResponse } from '../../models';

/* tslint:disable:max-classes-per-file*/
export enum VenueDetailsActionsTypes {
  Load = '[VenueDetails] Load venue details',
  Update = '[VenueDetails] Update venue details',
  Fail = '[VenueDetails] Fail retrieving venue details',
  Select = '[VenueDetails] Select one venue details',
}

export class LoadVenueDetails implements ICustomAsyncAction<IVenueDetailsResponse> {
  readonly type = VenueDetailsActionsTypes.Load;

  constructor(public asyncPayload: Promise<IVenueDetailsResponse>) {
  }
}

export class UpdateVenueDetails implements ICustomAction<IVenueDetails> {
  readonly type = VenueDetailsActionsTypes.Update;

  constructor(public payload: IVenueDetails) {
  }
}

export class FailVenueDetails implements IBaseAction {
  readonly type = VenueDetailsActionsTypes.Fail;

  constructor(public error: any) {
  }
}

export class SelectVenueDetails implements ICustomAction<string> {
  readonly type = VenueDetailsActionsTypes.Select;

  constructor(public payload: string) {
  }
}

export type VenueDetailsActions = LoadVenueDetails | UpdateVenueDetails | FailVenueDetails | SelectVenueDetails;
/* tslint:enable:max-classes-per-file*/
