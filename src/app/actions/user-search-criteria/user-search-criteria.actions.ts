import { Action } from '@ngrx/store';
import { ICustomAction } from '../../common/actions/custom-action';
import { IUserSearchCriteria } from '../../models';

/* tslint:disable:max-classes-per-file*/
export enum UserSearchCriteriaActionsTypes {
  NewSearchCriteria = '[UserSearchCriteria] Set new search criteria',
  ClearSearchCriteria = '[UserSearchCriteria] Clear existing search criteria',
}

export class NewSearchCriteria implements ICustomAction<IUserSearchCriteria> {
  readonly type = UserSearchCriteriaActionsTypes.NewSearchCriteria;

  constructor(public payload: IUserSearchCriteria) {
  }
}

export class ClearSearchCriteria implements Action {
  readonly type = UserSearchCriteriaActionsTypes.ClearSearchCriteria;
}

export type UserSearchCriteriaActions = NewSearchCriteria | ClearSearchCriteria;
/* tslint:enable:max-classes-per-file*/
