import { IBaseAction } from '../../common/actions/base-action';
import { ICustomAction } from '../../common/actions/custom-action';
import { ICustomAsyncAction } from '../../common/actions/custom-async-action';
import { IVenue, IVenueSearchResponse } from '../../models/foursquare';

/* tslint:disable:max-classes-per-file*/
export enum VenuesSearchActionsTypes {
  Load = '[VenuesSearch] Load venues search',
  Update = '[VenuesSearch] Update venues search',
  Fail = '[VenuesSearch] Fail retrieving venues search',
}

export class LoadVenuesSearch implements ICustomAsyncAction<IVenueSearchResponse> {
  readonly type = VenuesSearchActionsTypes.Load;

  constructor(public asyncPayload: Promise<IVenueSearchResponse>) {
  }
}

export class UpdateVenuesSearch implements ICustomAction<IVenue[]> {
  readonly type = VenuesSearchActionsTypes.Update;

  constructor(public payload: IVenue[]) {
  }
}

export class FailVenuesSearch implements IBaseAction {
  readonly type = VenuesSearchActionsTypes.Fail;

  constructor(public error: any) {
  }
}

export type VenuesSearchActions = LoadVenuesSearch | UpdateVenuesSearch | FailVenuesSearch;
/* tslint:enable:max-classes-per-file*/
