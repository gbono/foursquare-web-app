import { IBaseAction } from '../../common/actions/base-action';
import { ICustomAction } from '../../common/actions/custom-action';
import { ICustomAsyncAction } from '../../common/actions/custom-async-action';
import { IVenueCategoriesResponse, IVenueCategory } from '../../models';

/* tslint:disable:max-classes-per-file*/
export enum VenuesCategoriesActionsTypes {
  Load = '[VenuesCategories] Load venues categories',
  Update = '[VenuesCategories] Update venues categories',
  Fail = '[VenuesCategories] Fail retrieving venues categories',
}

export class LoadVenuesCategories implements ICustomAsyncAction<IVenueCategoriesResponse> {
  readonly type = VenuesCategoriesActionsTypes.Load;

  constructor(public asyncPayload: Promise<IVenueCategoriesResponse>) {
  }
}

export class UpdateVenuesCategories implements ICustomAction<IVenueCategory[]> {
  readonly type = VenuesCategoriesActionsTypes.Update;

  constructor(public payload: IVenueCategory[]) {
  }
}

export class FailVenuesCategories implements IBaseAction {
  readonly type = VenuesCategoriesActionsTypes.Fail;

  constructor(public error: any) {
  }
}

export type VenuesCategoriesActions = LoadVenuesCategories | UpdateVenuesCategories | FailVenuesCategories;
/* tslint:enable:max-classes-per-file*/
