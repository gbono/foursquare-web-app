import { IBaseAction } from '../../common/actions/base-action';
import { ICustomAction } from '../../common/actions/custom-action';
import { ICustomAsyncAction } from '../../common/actions/custom-async-action';
import { IGeoCode } from '../../models/geo-code/geo-code';
import { IGeoCodeResponse } from '../../models/geo-code/geo-code-response';

/* tslint:disable:max-classes-per-file*/
export enum GeoCodeActionsTypes {
  Load = '[GeoCode] Load geo code',
  Update = '[GeoCode] Update geo code',
  Fail = '[GeoCode] Fail retrieving geo code',
}

export class LoadGeoCode implements ICustomAsyncAction<IGeoCodeResponse> {
  readonly type = GeoCodeActionsTypes.Load;

  constructor(public asyncPayload: Promise<IGeoCodeResponse>) {
  }
}

export class UpdateGeoCode implements ICustomAction<IGeoCode[]> {
  readonly type = GeoCodeActionsTypes.Update;

  constructor(public payload: IGeoCode[]) {
  }
}

export class FailGeoCode implements IBaseAction {
  readonly type = GeoCodeActionsTypes.Fail;

  constructor(public error: any) {
  }
}

export type GeoCodeActions = LoadGeoCode | UpdateGeoCode | FailGeoCode;
/* tslint:enable:max-classes-per-file*/
