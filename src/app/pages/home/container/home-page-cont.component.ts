import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StateStatus } from '../../../common/actions/state-status';
import { IUserSearchCriteria } from '../../../models/user-search-criteria';
import { GeoCodeService } from '../../../services/geo-code/geo-code.service';
import { UserSearchCriteriaService } from '../../../services/user-search-criteria/user-search-criteria.service';
import { VenuesCategoriesService } from '../../../services/venues-categories/venues-categories.service';

@Component({
  selector: 'app-home-page-cont',
  templateUrl: './home-page-cont.component.html',
  styleUrls: ['./home-page-cont.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePageContComponent implements OnInit {

  public stateStatus = StateStatus;

  constructor(public venuesCategoriesService: VenuesCategoriesService,
              public geoCodeService: GeoCodeService,
              private _userSearchCriteriaService: UserSearchCriteriaService,
              private _router: Router) {
  }

  public ngOnInit() {
    this.venuesCategoriesService.loadVenuesCategories();
    this.geoCodeService.findUserLocation();
  }

  public onSearch(userSearchCriteria: IUserSearchCriteria): void {
    this._userSearchCriteriaService.setNewSearchCriteria({ ...userSearchCriteria });
    this._router.navigate(['search']);
  }
}
