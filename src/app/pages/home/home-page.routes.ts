import { Routes } from '@angular/router';
import { HomePageContComponent } from './container/home-page-cont.component';

export const routes: Routes = [
  {
    path: '',
    component: HomePageContComponent,
  },
];
