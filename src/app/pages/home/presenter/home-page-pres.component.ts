import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { IUserSearchCriteria, IVenueCategory } from '../../../models';
import { IGeoCode } from '../../../models/geo-code';

@Component({
  selector: 'app-home-page-pres',
  templateUrl: './home-page-pres.component.html',
  styleUrls: ['./home-page-pres.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePagePresComponent implements OnInit, OnChanges {

  @Input()
  public venuesCategories: IVenueCategory[];

  @Input()
  public showLoading: boolean;

  @Input()
  public userLocation: IGeoCode[];

  @Output()
  public searchVenues: EventEmitter<IUserSearchCriteria> = new EventEmitter<IUserSearchCriteria>();

  public locationFormControl: FormControl;

  private userSearchCriteria: IUserSearchCriteria;

  ngOnInit() {
    this.locationFormControl = new FormControl('', Validators.required);
    this.userSearchCriteria = { location: '', categoryId: '' };
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges['userLocation'] &&
      simpleChanges['userLocation'].previousValue !== simpleChanges['userLocation'].currentValue) {
      if (this.locationFormControl && this.userLocation[0]) {
        this.locationFormControl.setValue(this.userLocation[0].address_components[2].long_name);
      }
    }
  }

  public search(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.userSearchCriteria.location = this.locationFormControl.value;
    this.searchVenues.emit(this.userSearchCriteria);
  }
}
