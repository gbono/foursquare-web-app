import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IVenue } from '../../../models/foursquare/venues-search';

@Component({
  selector: 'app-search-page-pres',
  templateUrl: './search-page-pres.component.html',
  styleUrls: ['./search-page-pres.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchPagePresComponent {

  @Input()
  public venues: IVenue[];
}
