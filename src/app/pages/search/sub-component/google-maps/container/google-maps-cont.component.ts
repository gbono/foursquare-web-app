import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IVenue } from '../../../../../models';
import { VenueDetailsService } from '../../../../../services/venue-details/venue-details.service';

@Component({
  selector: 'app-google-maps-cont',
  templateUrl: './google-maps-cont.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GoogleMapsContComponent {

  @Input()
  public venues: IVenue[];

  constructor(public venueDetailsService: VenueDetailsService) {
  }
}
