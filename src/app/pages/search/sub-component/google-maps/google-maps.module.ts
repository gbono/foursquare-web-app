import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleMapsContComponent } from './container/google-maps-cont.component';
import { GoogleMapsPresComponent } from './presenter/google-maps-pres.component';

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyATCWZqTwZUA3iJVeO_OwoCzHDQtf5Sle8',
    }),
  ],
  declarations: [GoogleMapsPresComponent, GoogleMapsContComponent],
  exports: [GoogleMapsContComponent],
})
export class GoogleMapsModule {
}
