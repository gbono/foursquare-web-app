import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IVenue } from '../../../../../models';

@Component({
  selector: 'app-google-maps-pres',
  templateUrl: './google-maps-pres.component.html',
  styleUrls: ['./google-maps-pres.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GoogleMapsPresComponent {

  @Input()
  public venues: IVenue[];

  @Input()
  public forcedCenterLatitude?: number = undefined;

  @Input()
  public forcedCenterLongitude?: number = undefined;

  @Input()
  public forcedZoomLevel?: number = undefined;
}
