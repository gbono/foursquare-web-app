import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { IVenueDetails } from '../../../../../models/index';

@Component({
  selector: 'app-venue-card-pres',
  templateUrl: './venue-card-pres.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VenueCardPresComponent {

  @Input()
  public venueDetails: IVenueDetails;

  @Output()
  public venueCardClick: EventEmitter<string> = new EventEmitter<string>();

  public focusVenueOnMap(): void {
    this.venueCardClick.emit(this.venueDetails.id);
  }
}
