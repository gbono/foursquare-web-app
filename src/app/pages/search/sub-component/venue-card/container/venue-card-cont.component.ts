import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { IVenue } from '../../../../../models/index';
import { VenueDetailsService } from '../../../../../services/venue-details/venue-details.service';

@Component({
  selector: 'app-venue-card-cont',
  templateUrl: './venue-card-cont.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VenueCardContComponent implements OnInit {

  @Input()
  public venue: IVenue;

  constructor(public venueDetailsService: VenueDetailsService) {
  }

  ngOnInit() {
    this.venueDetailsService.loadVenueDetails(this.venue.id);
  }

  public venueCardClick(venueDetailsId: string): void {
    this.venueDetailsService.selectVenueDetails(venueDetailsId);
  }
}
