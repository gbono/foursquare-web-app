import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { VenueCardContComponent } from './container/venue-card-cont.component';
import { VenueCardPresComponent } from './presenter/venue-card-pres.component';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatButtonModule,
  ],
  declarations: [VenueCardPresComponent, VenueCardContComponent],
  exports:  [VenueCardContComponent],
})
export class VenueCardModule { }
