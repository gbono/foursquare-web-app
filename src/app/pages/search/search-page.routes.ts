import { Routes } from '@angular/router';
import { SearchPageContComponent } from './container/search-page-cont.component';

export const routes: Routes = [
  {
    path: '',
    component: SearchPageContComponent,
  },
];
