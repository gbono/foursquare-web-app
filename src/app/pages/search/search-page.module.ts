import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SearchPageContComponent } from './container/search-page-cont.component';
import { SearchPagePresComponent } from './presenter/search-page-pres.component';
import { routes } from './search-page.routes';
import { GoogleMapsModule } from './sub-component/google-maps/google-maps.module';
import { VenueCardModule } from './sub-component/venue-card/venue-card.module';

@NgModule({
  imports: [
    CommonModule,
    GoogleMapsModule,
    VenueCardModule,
    MatProgressSpinnerModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SearchPagePresComponent, SearchPageContComponent],
  exports: [SearchPageContComponent],
})
export class SearchPageModule {
}
