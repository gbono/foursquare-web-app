import { ChangeDetectionStrategy, Component } from '@angular/core';
import { VenuesSearchService } from '../../../services/venues-search/venues-search.service';

@Component({
  selector: 'app-search-page-cont',
  templateUrl: './search-page-cont.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchPageContComponent {

  constructor(public venuesSearchService: VenuesSearchService) {
  }
}
