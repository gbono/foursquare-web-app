import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {path: 'home', loadChildren: './pages/home/home-page.module#HomePageModule'},
  {path: 'search', loadChildren: './pages/search/search-page.module#SearchPageModule'},
  {path: '**', redirectTo: 'home', pathMatch: 'full'},
];
