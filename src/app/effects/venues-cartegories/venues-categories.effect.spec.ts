import { TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { LoadVenuesCategories, VenuesCategoriesActionsTypes } from '../../actions/venues-categories/venues-categories.actions';
import { DataProviderService } from '../../services/data-provider/data-provider.service';
import { MockDataProviderService } from '../../services/data-provider/mock/mock-data-provider.service';
import { VenuesCategoriesEffect } from './venues-categories.effect';

describe('VenuesCategories effect', () => {
  let metadata: EffectsMetadata<VenuesCategoriesEffect>;
  let effects: VenuesCategoriesEffect;
  let dataProvider: DataProviderService;
  /* tslint:disable:prefer-const */
  let actions: Observable<any>;
  /* tslint:enable:prefer-const */
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch'),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        VenuesCategoriesEffect,
        provideMockActions(() => actions),
        { provide: Store, useValue: mockStore },
        { provide: DataProviderService, useClass: MockDataProviderService },
      ],
    });
    effects = TestBed.get(VenuesCategoriesEffect);
    dataProvider = TestBed.get(DataProviderService);

    metadata = getEffectsMetadata(effects);
  });

  it('should register load$ that dispatches an action', () => {
    expect(metadata.load$).toEqual({ dispatch: true });
  });

  it('should do an UPDATE if succeeded', () => {
    const action = new ReplaySubject(1);
    action.next(new LoadVenuesCategories(dataProvider.loadVenuesCategories()));

    effects.load$.subscribe((result: Action) => {
      expect(result.type).toEqual(VenuesCategoriesActionsTypes.Update);
    });
  });
});
