import { Injectable } from '@angular/core';
import { Actions as RxActions, Effect, ofType } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import {
  FailVenuesCategories,
  LoadVenuesCategories, UpdateVenuesCategories, VenuesCategoriesActionsTypes,
} from '../../actions/venues-categories/venues-categories.actions';
import { IVenueCategoriesResponse } from '../../models/foursquare';

@Injectable()
export class VenuesCategoriesEffect {

  @Effect()
  load$ = this.actions$.pipe(ofType(VenuesCategoriesActionsTypes.Load),
    mergeMap((action: LoadVenuesCategories) =>
      from(action.asyncPayload).pipe(
        map((reply: IVenueCategoriesResponse) => new UpdateVenuesCategories(reply.response.categories)),
        catchError((err) => of(new FailVenuesCategories(err))),
      ),
    ));

  constructor(protected actions$: RxActions) {}
}
