import { Injectable } from '@angular/core';
import { Actions as RxActions, Effect, ofType } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { FailGeoCode, GeoCodeActionsTypes, LoadGeoCode, UpdateGeoCode } from '../../actions/geo-code/geo-code.actions';
import { IGeoCodeResponse } from '../../models/geo-code';

@Injectable()
export class GeoCodeEffect {

  @Effect()
  load$ = this.actions$.pipe(ofType(GeoCodeActionsTypes.Load),
    mergeMap((action: LoadGeoCode) =>
      from(action.asyncPayload).pipe(
        map((reply: IGeoCodeResponse) => new UpdateGeoCode(reply.results)),
        catchError((err) => of(new FailGeoCode(err))),
      ),
    ));

  constructor(protected actions$: RxActions) {
  }
}
