import { Injectable } from '@angular/core';
import { Actions as RxActions, Effect, ofType } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {
  FailVenueDetails, LoadVenueDetails, UpdateVenueDetails, VenueDetailsActionsTypes,
} from '../../actions/venue-details/venue-details.actions';
import { IVenueDetailsResponse } from '../../models/foursquare/venue-details/venue-details-response';

@Injectable()
export class VenueDetailsEffect {

  @Effect()
  load$ = this.actions$.pipe(ofType(VenueDetailsActionsTypes.Load),
    mergeMap((action: LoadVenueDetails) =>
      from(action.asyncPayload).pipe(
        map((reply: IVenueDetailsResponse) => new UpdateVenueDetails(reply.response.venue)),
        catchError((err) => of(new FailVenueDetails(err))),
      ),
    ));

  constructor(protected actions$: RxActions) {
  }
}
