import { TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { LoadVenueDetails, VenueDetailsActionsTypes } from '../../actions/venue-details/venue-details.actions';
import { DataProviderService } from '../../services/data-provider/data-provider.service';
import { MockDataProviderService } from '../../services/data-provider/mock/mock-data-provider.service';
import { VenueDetailsEffect } from './venue-details.effect';

describe('VenueDetails effect', () => {
  let metadata: EffectsMetadata<VenueDetailsEffect>;
  let effects: VenueDetailsEffect;
  let dataProvider: DataProviderService;
  /* tslint:disable:prefer-const */
  let actions: Observable<any>;
  /* tslint:enable:prefer-const */
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch'),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        VenueDetailsEffect,
        provideMockActions(() => actions),
        { provide: Store, useValue: mockStore },
        { provide: DataProviderService, useClass: MockDataProviderService },
      ],
    });
    effects = TestBed.get(VenueDetailsEffect);
    dataProvider = TestBed.get(DataProviderService);

    metadata = getEffectsMetadata(effects);
  });

  it('should register load$ that dispatches an action', () => {
    expect(metadata.load$).toEqual({ dispatch: true });
  });

  it('should do an UPDATE if succeeded', () => {
    const action = new ReplaySubject(1);
    action.next(new LoadVenueDetails(dataProvider.loadVenueDetails('test')));

    effects.load$.subscribe((result: Action) => {
      expect(result.type).toEqual(VenueDetailsActionsTypes.Update);
    });
  });
});
