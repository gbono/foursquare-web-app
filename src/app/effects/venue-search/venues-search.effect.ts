import { Injectable } from '@angular/core';
import { Actions as RxActions, Effect, ofType } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { NewSearchCriteria, UserSearchCriteriaActionsTypes } from '../../actions/user-search-criteria/user-search-criteria.actions';
import {
  FailVenuesSearch, LoadVenuesSearch, UpdateVenuesSearch, VenuesSearchActionsTypes,
} from '../../actions/venues-serach/venues-search.actions';
import { IVenueSearchResponse } from '../../models/foursquare';
import { DataProviderService } from '../../services/data-provider/data-provider.service';

@Injectable()
export class VenuesSearchEffect {

  @Effect()
  load$ = this.actions$.pipe(ofType(VenuesSearchActionsTypes.Load),
    mergeMap((action: LoadVenuesSearch) =>
      from(action.asyncPayload).pipe(
        map((reply: IVenueSearchResponse) => new UpdateVenuesSearch(reply.response.venues)),
        catchError((err) => of(new FailVenuesSearch(err))),
      ),
    ),
  );

  @Effect()
  search$ = this.actions$.pipe(ofType(UserSearchCriteriaActionsTypes.NewSearchCriteria),
    map((action: NewSearchCriteria) =>
      new LoadVenuesSearch(this._dataProviderService.searchVenues(action.payload)),
    ));

  constructor(protected actions$: RxActions,
              private _dataProviderService: DataProviderService) {
  }
}
