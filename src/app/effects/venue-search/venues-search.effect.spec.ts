import { TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { NewSearchCriteria } from '../../actions/user-search-criteria/user-search-criteria.actions';
import { LoadVenuesSearch, VenuesSearchActionsTypes } from '../../actions/venues-serach/venues-search.actions';
import { DataProviderService } from '../../services/data-provider/data-provider.service';
import { MockDataProviderService } from '../../services/data-provider/mock/mock-data-provider.service';
import { VenuesSearchEffect } from './venues-search.effect';

describe('VenuesSearch effect', () => {
  let metadata: EffectsMetadata<VenuesSearchEffect>;
  let effects: VenuesSearchEffect;
  let dataProvider: DataProviderService;
  /* tslint:disable:prefer-const */
  let actions: Observable<any>;
  /* tslint:enable:prefer-const */
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch'),
  };

  beforeEach(() => {
    TestBed.configureTestingModule( {
      providers: [
        VenuesSearchEffect,
        provideMockActions(() => actions),
        { provide: Store, useValue: mockStore },
        { provide: DataProviderService, useClass: MockDataProviderService },
      ],
    });
    effects = TestBed.get(VenuesSearchEffect);
    dataProvider = TestBed.get(DataProviderService);

    metadata = getEffectsMetadata(effects);
  });

  it('should register load$ that dispatches an action', () => {
    expect(metadata.load$).toEqual({ dispatch: true });
  });

  it('should register search$ that dispatches an action', () => {
    expect(metadata.search$).toEqual({ dispatch: true });
  });

  it('should do an UPDATE if succeeded', () => {
    const action = new ReplaySubject(1);
    action.next(new LoadVenuesSearch(dataProvider.searchVenues({ categoryId: 'fake', location: 'test' })));

    effects.load$.subscribe((result: Action) => {
      expect(result.type).toEqual(VenuesSearchActionsTypes.Update);
    });
  });

  it('should do a SEARCH after an UPDATE search criteria if succeeded', () => {
    const action = new ReplaySubject(1);
    action.next(new NewSearchCriteria({ categoryId: 'test', location: 'test' }));

    effects.load$.subscribe((result: Action) => {
      expect(result.type).toEqual(VenuesSearchActionsTypes.Load);
    });
  });
});
