import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { PageHeaderPresComponent } from './presenter/navigation-bar-pres.component';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
  ],
  declarations: [PageHeaderPresComponent],
  exports: [PageHeaderPresComponent],
})
export class NavigationBarModule {
}
