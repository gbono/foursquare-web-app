import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {PageHeaderPresComponent} from './presenter/page-header-pres.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [PageHeaderPresComponent],
  exports: [PageHeaderPresComponent],
})
export class PageHeaderModule {
}
