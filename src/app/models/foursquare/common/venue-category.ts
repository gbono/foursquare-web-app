import { IVenueCategoryIcon } from '../venue-categories/venue-category-icon';

export interface IVenueCategory {
  id: string;
  name: string;
  pluralName: string;
  shortName: string;
  icon: IVenueCategoryIcon;
  categories: IVenueCategory[];
}
