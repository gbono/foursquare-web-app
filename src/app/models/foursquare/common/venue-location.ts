export interface IVenueLocation {
  address: string;
  crossStreet: string;
  lat: number;
  lng: number;
  labeledLatLngs: Array<{ label: string, lat: number; lng: number }>;
  distance: number;
  postalCode: string;
  cc: string;
  city: string;
  state: string;
  country: string;
  formattedAddress: string[];
}
