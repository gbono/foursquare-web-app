export interface IMeta {
  code: number;
  requestId: string;
}
