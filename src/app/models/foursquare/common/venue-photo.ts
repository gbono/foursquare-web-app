export interface IVenuePhoto {
  id: string;
  createdAt: number;
  source: {
    name: string;
    url: string;
  };
  prefix: string;
  suffix: string;
  width: number;
  height: number;
  user: {
    id: string;
    firstName: string;
    lastName: string;
    gender: string;
    photo: {
      prefix: string;
      suffix: string;
    }
  };
}
