import { IVenueCategory } from '..';

export interface IVenueSearchCategory extends IVenueCategory {
  primary: boolean;
}
