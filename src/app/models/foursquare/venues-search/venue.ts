import { IVenueLocation } from '../common/venue-location';
import { IVenueSearchCategory } from './venue-search-category';

export interface IVenue {
  id: string;
  name: string;
  location: IVenueLocation;
  categories: IVenueSearchCategory[];
  venuePage: { id: number };
}
