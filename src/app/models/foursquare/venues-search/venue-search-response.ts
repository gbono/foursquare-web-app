import { IMeta } from '../common';
import { IVenue } from './venue';

export interface IVenueSearchResponse {
  meta: IMeta;
  response: {
    venues: IVenue[],
  };
}
