import { IMeta } from '../common';
import { IVenueTip } from './venue-tip';

export interface IVenueTipsResponse {
  meta: IMeta;
  response: {
    tips: {
      count: number;
      items: IVenueTip[];
    },
  };
}
