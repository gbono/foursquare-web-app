export interface IVenueTip {
  id: string;
  createdAt: number;
  text: string;
  type: string;
  canonicalUrl: string;
  photo: {
    id: string;
    createdAt: number;
    source: {
      name: string;
      url: string;
    },
    prefix: string;
    suffix: string;
    width: number;
    height: number;
    visibility: string;
  };
  photourl: string;
  likes: {
    count: number;
    groups: Array<{type: string; count: number, items: any[]}>;
  };
  like: boolean;
  logView: boolean;
  agreeCount: number;
  disagreeCount: number;
  todo: {
    count: number;
  };
  user: {
    id: number;
    firstName: string;
    lastName: string;
    gender: string;
    photo: {
      prefix: string;
      suffix: string;
    };
    authorInteractionType: string;
  };
}
