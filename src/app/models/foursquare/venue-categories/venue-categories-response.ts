import { IMeta } from '../common/meta';
import { IVenueCategory } from '../common/venue-category';

export interface IVenueCategoriesResponse {
  meta: IMeta;
  response: { categories: IVenueCategory[] };
}
