export interface IVenueCategoryIcon {
  prefix: string;
  suffix: string;
}
