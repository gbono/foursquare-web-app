export interface IVenueContact {
  phone: string;
  formattedPhone: string;
  twitter: string;
  instagram: string;
  facebook: string;
  facebookUserName: string;
  facebookName: string;
}
