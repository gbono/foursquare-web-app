import { IMeta } from '../common';
import { IVenueDetails } from './venue-details';

export interface IVenueDetailsResponse {
  meta: IMeta;
  response: {
    venue: IVenueDetails,
  };
}
