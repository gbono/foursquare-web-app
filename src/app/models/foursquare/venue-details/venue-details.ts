import { IVenueLocation } from '../common';
import { IVenuePhoto } from '../common/venue-photo';
import { IVenueTip } from '../venue-tips/venue-tip';
import { IVenueSearchCategory } from '../venues-search';
import { IVenueBeenHere } from './venue-been-here';
import { IVenueContact } from './venue-contact';
import { IVenueLikes } from './venue-likes';
import { IVenueStats } from './venue-stats';

export interface IVenueDetails {
  id: string;
  name: string;
  contact: IVenueContact;
  location: IVenueLocation;
  canonicalUrl: string;
  categories: IVenueSearchCategory[];
  verified: boolean;
  stats: IVenueStats;
  url: string;
  likes: IVenueLikes;
  rating: number;
  ratingColor: string;
  ratingSignals: number;
  beenHere: IVenueBeenHere;
  photos: {
    count: number;
    groups: Array<{type: string; name: string; count: number; items: IVenuePhoto[]}>
  };
  description: string;
  storeId: string;
  hereNow: {
    count: number;
    summary: string;
  };
  tips: IVenueTip[];
  popular: {
    isOpen: boolean;
  };
  bestPhoto: {
    id: string,
    createdAt: number,
    source: {
      name: string,
      url: string,
    };
    prefix: string;
    suffix: string,
    width: number,
    height: number,
    visibility: string,
  };
}
