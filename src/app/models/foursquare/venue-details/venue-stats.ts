export interface IVenueStats {
  checkinsCount: number;
  usersCount: number;
  tipCount: number;
  visitsCount: number;
}
