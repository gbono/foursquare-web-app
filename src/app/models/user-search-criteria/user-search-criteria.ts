export interface IUserSearchCriteria {
  location: string;
  categoryId: string;
}
