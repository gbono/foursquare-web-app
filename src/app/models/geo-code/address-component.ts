export interface IAddressComponent {
  long_name: number;
  short_name: number;
  types: string[];
}
