import { IAddressComponent } from './address-component';
import { IGeometry } from './geometry';

export interface IGeoCode {
  address_components: IAddressComponent;
  formatted_address: string;
  geometry: IGeometry;
  place_id: string;
  types: string[];
}
