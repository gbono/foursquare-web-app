import { ILocation } from './location';

export interface IGeometry {
  location: ILocation;
  location_type: string;
  viewport: {
    northeast: ILocation,
    southwest: ILocation,
  };
}
