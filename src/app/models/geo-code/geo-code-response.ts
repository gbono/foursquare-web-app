import { IGeoCode } from './geo-code';

export interface IGeoCodeResponse {
  results: IGeoCode[];
}
