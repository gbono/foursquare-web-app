import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';
import { CustomRouterStateSerializer } from './common/custom-router-serializer';
import { PageHeaderModule } from './components/shared/page-header/page-header.module';
import { GeoCodeEffect } from './effects/geo-code/geo-code.effect';
import { VenueDetailsEffect } from './effects/venue-details/venue-details.effect';
import { VenuesSearchEffect } from './effects/venue-search/venues-search.effect';
import { VenuesCategoriesEffect } from './effects/venues-cartegories/venues-categories.effect';
import { metaReducers, reducers } from './reducers';
import { DataProviderService } from './services/data-provider/data-provider.service';
import { GeoCodeService } from './services/geo-code/geo-code.service';
import { RequestsInterceptorService } from './services/requests-interceptor/requests-interceptor.service';
import { UserSearchCriteriaService } from './services/user-search-criteria/user-search-criteria.service';
import { VenueDetailsService } from './services/venue-details/venue-details.service';
import { VenuesCategoriesService } from './services/venues-categories/venues-categories.service';
import { VenuesSearchService } from './services/venues-search/venues-search.service';

const COMPONENTS_MODULES = [
  PageHeaderModule,
];

const SERVICES = [
  DataProviderService,
  { provide: HTTP_INTERCEPTORS, useClass: RequestsInterceptorService, multi: true },
  { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer },
  VenuesCategoriesService,
  GeoCodeService,
  VenuesSearchService,
  UserSearchCriteriaService,
  VenueDetailsService,
];

const EFFECTS = [
  VenuesCategoriesEffect,
  GeoCodeEffect,
  VenuesSearchEffect,
  VenueDetailsEffect,
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(ROUTES),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
    }),
    EffectsModule.forRoot([...EFFECTS]),
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    COMPONENTS_MODULES,
  ],
  providers: [
    SERVICES,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
