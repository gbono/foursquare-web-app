import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { NewSearchCriteria } from '../../actions/user-search-criteria/user-search-criteria.actions';
import { DataProviderService } from '../data-provider/data-provider.service';
import { MockDataProviderService } from '../data-provider/mock/mock-data-provider.service';
import { UserSearchCriteriaService } from './user-search-criteria.service';

describe('UserSearchCriteria service', () => {
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch'),
  };
  let userSearchCriteriaService: UserSearchCriteriaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        UserSearchCriteriaService,
        {provide: DataProviderService, useClass: MockDataProviderService},
        {provide: Store, useValue: mockStore},
      ],
    });
    userSearchCriteriaService = TestBed.get(UserSearchCriteriaService);
  });

  it('should be created', () => {
    expect(userSearchCriteriaService).toBeTruthy();
  });

  it('should set new search criteria', async (done) => {
    userSearchCriteriaService.setNewSearchCriteria({categoryId: 'test', location: 'test'});
    expect(mockStore.dispatch).toHaveBeenCalledWith(new NewSearchCriteria({categoryId: 'test', location: 'test'}));
    done();
  });
});
