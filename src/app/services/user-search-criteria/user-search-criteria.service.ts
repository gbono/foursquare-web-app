import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';
import { NewSearchCriteria } from '../../actions/user-search-criteria/user-search-criteria.actions';
import { IUserSearchCriteria } from '../../models/user-search-criteria';
import * as fromRoot from '../../reducers';
import { userSearchCriteria } from '../../selectors/user-search-criteria/user-search-criteria.selectors';

@Injectable()
export class UserSearchCriteriaService {

  private readonly _userSearchCriteria$: Observable<IUserSearchCriteria>;

  constructor(private _store: Store<fromRoot.IState>) {
    this._userSearchCriteria$ = this._store.select(userSearchCriteria);
  }

  public get userSearchCriteria$(): Observable<IUserSearchCriteria> {
    return this._userSearchCriteria$;
  }

  public setNewSearchCriteria(searchCriteria: IUserSearchCriteria): void {
    this._store.dispatch(new NewSearchCriteria(searchCriteria));
  }
}
