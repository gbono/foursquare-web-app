import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { DataProviderService } from '../data-provider/data-provider.service';
import { MockDataProviderService } from '../data-provider/mock/mock-data-provider.service';
import { GeoCodeService } from './geo-code.service';

describe('GeoCode service', () => {
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch'),
  };
  let geoCodeService: GeoCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        GeoCodeService,
        {provide: DataProviderService, useClass: MockDataProviderService},
        {provide: Store, useValue: mockStore},
      ],
    });
    geoCodeService = TestBed.get(GeoCodeService);
  });

  it('should be created', () => {
    expect(geoCodeService).toBeTruthy();
  });

  it('should convert geo position into physical address', async (done) => {
    geoCodeService.convertPositionIntoAddress(1212, 1313);
    expect(mockStore.dispatch).toHaveBeenCalled();
    done();
  });
});
