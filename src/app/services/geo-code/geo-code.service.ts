import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';
import { LoadGeoCode } from '../../actions/geo-code/geo-code.actions';
import { IGeoCode } from '../../models';
import * as fromRoot from '../../reducers';
import { selectAllGeoCode } from '../../selectors/geo-code/geo-code.selectors';
import { DataProviderService } from '../data-provider/data-provider.service';

@Injectable()
export class GeoCodeService {

  private readonly _geoCodeResponse$: Observable<IGeoCode[]>;

  constructor(private _store: Store<fromRoot.IState>,
              private _dataProvider: DataProviderService) {
    this._geoCodeResponse$ = this._store.select(selectAllGeoCode);
  }

  public get geoCodeResponse$(): Observable<IGeoCode[]> {
    return this._geoCodeResponse$;
  }

  public convertPositionIntoAddress(latitude: number, longitude: number): void {
    this._store.dispatch(new LoadGeoCode(this._dataProvider.convertPositionIntoAddress(latitude, longitude)));
  }

  public findUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.convertPositionIntoAddress(position.coords.latitude, position.coords.longitude);
      });
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }
}
