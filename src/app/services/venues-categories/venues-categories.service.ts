import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IVenueCategory} from '../../models';

import {Store} from '@ngrx/store';
import {LoadVenuesCategories} from '../../actions/venues-categories/venues-categories.actions';
import {StateStatus} from '../../common/actions/state-status';
import * as fromRoot from '../../reducers';
import {selectAllVenuesCategories, venuesCategoriesStateStatus} from '../../selectors/venues-categories/venues-categories.selectors';
import {DataProviderService} from '../data-provider/data-provider.service';

@Injectable()
export class VenuesCategoriesService {

  private readonly _venuesCategories$: Observable<IVenueCategory[]>;
  private readonly _venuesCategoriesStateStatus$: Observable<StateStatus>;

  constructor(private _store: Store<fromRoot.IState>,
              private _dataProvider: DataProviderService) {
    this._venuesCategories$ = this._store.select(selectAllVenuesCategories);
    this._venuesCategoriesStateStatus$ = this._store.select(venuesCategoriesStateStatus);
  }

  public get venuesCategories$(): Observable<IVenueCategory[]> {
    return this._venuesCategories$;
  }

  public get venuesCategoriesStateStatus$(): Observable<StateStatus> {
    return this._venuesCategoriesStateStatus$;
  }

  public loadVenuesCategories(): void {
    this._store.dispatch(new LoadVenuesCategories(this._dataProvider.loadVenuesCategories()));
  }
}
