import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { DataProviderService } from '../data-provider/data-provider.service';
import { MockDataProviderService } from '../data-provider/mock/mock-data-provider.service';
import { VenuesCategoriesService } from './venues-categories.service';

describe('VenuesCategories service', () => {
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch'),
  };
  let venuesCategoriesService: VenuesCategoriesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        VenuesCategoriesService,
        {provide: DataProviderService, useClass: MockDataProviderService},
        {provide: Store, useValue: mockStore},
      ],
    });
    venuesCategoriesService = TestBed.get(VenuesCategoriesService);
  });

  it('should be created', () => {
    expect(venuesCategoriesService).toBeTruthy();
  });

  it('should load venues categories', async (done) => {
    venuesCategoriesService.loadVenuesCategories();
    expect(mockStore.dispatch).toHaveBeenCalled();
    done();
  });
});
