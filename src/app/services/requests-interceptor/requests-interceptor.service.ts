import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class RequestsInterceptorService implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.startsWith('https://api.foursquare.com')) {
      request = request.clone({
        setParams: {
          client_id: `CY0RLDKPOZE2TFXUCLRQAP1FDM3B2WR0X14XLNSQDOGSOAHE  `,
          client_secret: 'NKISIZZYDDY3YRMAVKKXY232SZXK4OPGMEKERH3MCLHZ5RNW',
          v: '20180529',
        },
      });
    }
    return next.handle(request);
  }
}
