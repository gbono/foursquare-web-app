import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { DataProviderService } from '../data-provider/data-provider.service';
import { MockDataProviderService } from '../data-provider/mock/mock-data-provider.service';
import { VenuesSearchService } from './venues-search.service';

describe('VenuesSearch service', () => {
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch'),
  };
  let venuesSearchService: VenuesSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        VenuesSearchService,
        {provide: DataProviderService, useClass: MockDataProviderService},
        {provide: Store, useValue: mockStore},
      ],
    });
    venuesSearchService = TestBed.get(VenuesSearchService);
  });

  it('should be created', () => {
    expect(venuesSearchService).toBeTruthy();
  });

  it('should load venues categories', async (done) => {
    venuesSearchService.loadVenuesSearch({location: 'test', categoryId: 'test'});
    expect(mockStore.dispatch).toHaveBeenCalled();
    done();
  });
});
