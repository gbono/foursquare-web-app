import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';
import { LoadVenuesSearch } from '../../actions/venues-serach/venues-search.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IUserSearchCriteria, IVenue } from '../../models';
import * as fromRoot from '../../reducers';
import { selectAllVenuesSearch, venuesSearchStateStatus } from '../../selectors/venues-search/venues-search.selectors';
import { DataProviderService } from '../data-provider/data-provider.service';

@Injectable()
export class VenuesSearchService {

  private readonly _venuesSearch$: Observable<IVenue[]>;
  private readonly _venuesSearchStateStatus$: Observable<StateStatus>;

  constructor(private _store: Store<fromRoot.IState>,
              private _dataProvider: DataProviderService) {
    this._venuesSearch$ = this._store.select(selectAllVenuesSearch);
    this._venuesSearchStateStatus$ = this._store.select(venuesSearchStateStatus);
  }

  public get venuesSearch$(): Observable<IVenue[]> {
    return this._venuesSearch$;
  }

  public get venuesSearchStateStatus$(): Observable<StateStatus> {
    return this._venuesSearchStateStatus$;
  }

  public loadVenuesSearch(userSearchCriteria: IUserSearchCriteria): void {
    this._store.dispatch(new LoadVenuesSearch(this._dataProvider.searchVenues(userSearchCriteria)));
  }
}
