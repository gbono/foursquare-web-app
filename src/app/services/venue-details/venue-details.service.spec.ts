import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { DataProviderService } from '../data-provider/data-provider.service';
import { MockDataProviderService } from '../data-provider/mock/mock-data-provider.service';
import { VenueDetailsService } from './venue-details.service';

describe('UserSearchCriteria service', () => {
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch'),
  };
  let venueDetailsService: VenueDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        VenueDetailsService,
        {provide: DataProviderService, useClass: MockDataProviderService},
        {provide: Store, useValue: mockStore},
      ],
    });
    venueDetailsService = TestBed.get(VenueDetailsService);
  });

  it('should be created', () => {
    expect(venueDetailsService).toBeTruthy();
  });

  it('should load venue details', async (done) => {
    venueDetailsService.loadVenueDetails('test');
    expect(mockStore.dispatch).toHaveBeenCalled();
    done();
  });

  it('should select venue details', async (done) => {
    venueDetailsService.selectVenueDetails('test');
    expect(mockStore.dispatch).toHaveBeenCalled();
    done();
  });
});
