import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { LoadVenueDetails, SelectVenueDetails } from '../../actions/venue-details/venue-details.actions';
import { StateStatus } from '../../common/actions/state-status';
import { IVenueDetails } from '../../models';
import * as fromRoot from '../../reducers';
import {
  getSelectedVenueDetails,
  selectAllVenueDetails,
  venueDetailsById,
  venueDetailsStateStatus,
} from '../../selectors/venue-details/venues-details.selectors';
import { DataProviderService } from '../data-provider/data-provider.service';

@Injectable()
export class VenueDetailsService {

  private readonly _venueDetails$: Observable<IVenueDetails[]>;
  private readonly _venueDetailsStateStatus$: Observable<StateStatus>;
  private readonly _selectedVenueDetails$: Observable<IVenueDetails>;

  constructor(private _store: Store<fromRoot.IState>,
              private _dataProvider: DataProviderService) {
    this._venueDetails$ = this._store.select(selectAllVenueDetails);
    this._venueDetailsStateStatus$ = this._store.select(venueDetailsStateStatus);
    this._selectedVenueDetails$ = this._store.select(getSelectedVenueDetails);
  }

  public get venueDetails$(): Observable<IVenueDetails[]> {
    return this._venueDetails$;
  }

  public get venueDetailsStateStatus$(): Observable<StateStatus> {
    return this._venueDetailsStateStatus$;
  }

  public get selectedVenueDetails$(): Observable<IVenueDetails> {
    return this._selectedVenueDetails$;
  }

  public getVenueDetailsById(venueId: string): Observable<IVenueDetails> {
    return this._store.select(venueDetailsById(venueId));
  }

  public loadVenueDetails(venueId: string): void {
    this._store.dispatch(new LoadVenueDetails(this._dataProvider.loadVenueDetails(venueId)));
  }

  public selectVenueDetails(venueDetailsId: string): void {
    this._store.dispatch(new SelectVenueDetails(venueDetailsId));
  }
}
