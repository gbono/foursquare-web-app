import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  buildFindVenuesByCategoryIdAndLocationUrl, buildGetVenueDetailsByIdUrl, buildGetVenuesCategoriesUrl,
} from '../../common/utils/four-square-urls.utils';
import { buildGeoCodeUrl } from '../../common/utils/google-api-urls.utils';
import { IGeoCodeResponse, IUserSearchCriteria, IVenueCategoriesResponse, IVenueDetailsResponse, IVenueSearchResponse } from '../../models';

@Injectable()
export class DataProviderService {

  private BASE_FOUR_SQUARES_URL = 'https://api.foursquare.com/v2/venues';
  private BASE_GOOGLE_GEOCODE_URL = 'https://maps.googleapis.com/maps/api/geocode/';

  constructor(private http: HttpClient) {
  }

  public loadVenuesCategories(): Promise<IVenueCategoriesResponse> {
    return this.http.get<IVenueCategoriesResponse>(buildGetVenuesCategoriesUrl(this.BASE_FOUR_SQUARES_URL)).toPromise();
  }

  public convertPositionIntoAddress(latitude: number, longitude: number): Promise<IGeoCodeResponse> {
    return this.http.get<IGeoCodeResponse>(buildGeoCodeUrl(this.BASE_GOOGLE_GEOCODE_URL)(latitude, longitude)).toPromise();
  }

  public searchVenues(userSearchCriteria: IUserSearchCriteria): Promise<IVenueSearchResponse> {
    return this.http.get<IVenueSearchResponse>(
      buildFindVenuesByCategoryIdAndLocationUrl(this.BASE_FOUR_SQUARES_URL)
      (userSearchCriteria.categoryId ? userSearchCriteria.categoryId : '', userSearchCriteria.location)).toPromise();
  }

  public loadVenueDetails(venueId: string): Promise<IVenueDetailsResponse> {
    return this.http.get<IVenueDetailsResponse>(buildGetVenueDetailsByIdUrl(this.BASE_FOUR_SQUARES_URL)(venueId)).toPromise();
  }
}
