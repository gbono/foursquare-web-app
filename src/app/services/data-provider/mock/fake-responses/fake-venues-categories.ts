export const fakeVenuesCategories = {
  meta: {
    code: 200,
    requestId: '5854a3664434b94a0959cfb7',
  },
  notifications: [
    {
      item: {
        unreadCount: 13,
      },
      type: 'notificationTray',
    },
  ],
  response: {
    categories: [
      {
        categories: [
          {
            categories: [],
            icon: {
              prefix: 'https://ss3.4sqi.net/img/categories_v2/arts_entertainment/default_',
              suffix: '.png',
            },
            id: '56aa371be4b08b9a8d5734db',
            name: 'Amphitheater',
            pluralName: 'Amphitheaters',
            shortName: 'Amphitheater',
          },
        ],
      },
    ],
  },
};
