export const fakeGeoCodeResponse = {
  results: [
    {
      address_components: [
        {
          long_name: '3',
          short_name: '3',
          types: ['street_number'],
        },
        {
          long_name: 'Avenue Barquier',
          short_name: 'Avenue Barquier',
          types: ['route'],
        },
        {
          long_name: 'Antibes',
          short_name: 'Antibes',
          types: ['locality', 'political'],
        },
        {
          long_name: 'Alpes-Maritimes',
          short_name: 'Alpes-Maritimes',
          types: ['administrative_area_level_2', 'political'],
        },
        {
          long_name: 'Provence-Alpes-Côte d\'Azur',
          short_name: 'Provence-Alpes-Côte d\'Azur',
          types: ['administrative_area_level_1', 'political'],
        },
        {
          long_name: 'France',
          short_name: 'FR',
          types: ['country', 'political'],
        },
        {
          long_name: '06600',
          short_name: '06600',
          types: ['postal_code'],
        },
      ],
      formatted_address: '3 Avenue Barquier, 06600 Antibes, France',
      geometry: {
        location: {
          lat: 43.5769346,
          lng: 7.125513,
        },
        location_type: 'ROOFTOP',
        viewport: {
          northeast: {
            lat: 43.5782835802915,
            lng: 7.126861980291502,
          },
          southwest: {
            lat: 43.5755856197085,
            lng: 7.124164019708497,
          },
        },
      },
      place_id: 'ChIJXwmdznrVzRIRy622G40rZ3s',
      types: ['street_address'],
    },
    {
      address_components: [
        {
          long_name: 'Antibes',
          short_name: 'Antibes',
          types: ['locality', 'political'],
        },
        {
          long_name: 'Alpes-Maritimes',
          short_name: 'Alpes-Maritimes',
          types: ['administrative_area_level_2', 'political'],
        },
        {
          long_name: 'Provence-Alpes-Côte d\'Azur',
          short_name: 'Provence-Alpes-Côte d\'Azur',
          types: ['administrative_area_level_1', 'political'],
        },
        {
          long_name: 'France',
          short_name: 'FR',
          types: ['country', 'political'],
        },
      ],
      formatted_address: 'Antibes, France',
      geometry: {
        bounds: {
          northeast: {
            lat: 43.6227,
            lng: 7.145126899999999,
          },
          southwest: {
            lat: 43.541863,
            lng: 7.0645681,
          },
        },
        location: {
          lat: 43.58041799999999,
          lng: 7.125102,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          northeast: {
            lat: 43.6227,
            lng: 7.145126899999999,
          },
          southwest: {
            lat: 43.541863,
            lng: 7.0645681,
          },
        },
      },
      place_id: 'ChIJqZFankXVzRIRsJ-X_aUZCAQ',
      types: ['locality', 'political'],
    },
    {
      address_components: [
        {
          long_name: '06600',
          short_name: '06600',
          types: ['postal_code'],
        },
        {
          long_name: 'Antibes',
          short_name: 'Antibes',
          types: ['locality', 'political'],
        },
        {
          long_name: 'Alpes-Maritimes',
          short_name: 'Alpes-Maritimes',
          types: ['administrative_area_level_2', 'political'],
        },
        {
          long_name: 'Provence-Alpes-Côte d\'Azur',
          short_name: 'Provence-Alpes-Côte d\'Azur',
          types: ['administrative_area_level_1', 'political'],
        },
        {
          long_name: 'France',
          short_name: 'FR',
          types: ['country', 'political'],
        },
      ],
      formatted_address: '06600 Antibes, France',
      geometry: {
        bounds: {
          northeast: {
            lat: 43.6227904,
            lng: 7.132965,
          },
          southwest: {
            lat: 43.5726269,
            lng: 7.0644661,
          },
        },
        location: {
          lat: 43.5990157,
          lng: 7.103639599999999,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          northeast: {
            lat: 43.6227904,
            lng: 7.132965,
          },
          southwest: {
            lat: 43.5726269,
            lng: 7.0644661,
          },
        },
      },
      place_id: 'ChIJy132OjLVzRIRILe2UKkZCBw',
      types: ['postal_code'],
    },
    {
      address_components: [
        {
          long_name: 'Alpes-Maritimes',
          short_name: 'Alpes-Maritimes',
          types: ['administrative_area_level_2', 'political'],
        },
        {
          long_name: 'Provence-Alpes-Côte d\'Azur',
          short_name: 'Provence-Alpes-Côte d\'Azur',
          types: ['administrative_area_level_1', 'political'],
        },
        {
          long_name: 'France',
          short_name: 'FR',
          types: ['country', 'political'],
        },
      ],
      formatted_address: 'Alpes-Maritimes, France',
      geometry: {
        bounds: {
          northeast: {
            lat: 44.3611549,
            lng: 7.718992999999998,
          },
          southwest: {
            lat: 43.48030199999999,
            lng: 6.635411899999999,
          },
        },
        location: {
          lat: 43.9466791,
          lng: 7.179025999999999,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          northeast: {
            lat: 44.3611549,
            lng: 7.718992999999998,
          },
          southwest: {
            lat: 43.48030199999999,
            lng: 6.635411899999999,
          },
        },
      },
      place_id: 'ChIJj00oCCe0zRIR0CWP_aUZCAM',
      types: ['administrative_area_level_2', 'political'],
    },
    {
      address_components: [
        {
          long_name: 'Provence-Alpes-Côte d\'Azur',
          short_name: 'Provence-Alpes-Côte d\'Azur',
          types: ['administrative_area_level_1', 'political'],
        },
        {
          long_name: 'France',
          short_name: 'FR',
          types: ['country', 'political'],
        },
      ],
      formatted_address: 'Provence-Alpes-Côte d\'Azur, France',
      geometry: {
        bounds: {
          northeast: {
            lat: 45.12685099999999,
            lng: 7.718992999999998,
          },
          southwest: {
            lat: 42.98199810000001,
            lng: 4.230207,
          },
        },
        location: {
          lat: 43.9351691,
          lng: 6.0679194,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          northeast: {
            lat: 45.12685099999999,
            lng: 7.718992999999998,
          },
          southwest: {
            lat: 42.98199810000001,
            lng: 4.230207,
          },
        },
      },
      place_id: 'ChIJrVP5ihlothIRp9EWPSaQFrc',
      types: ['administrative_area_level_1', 'political'],
    },
    {
      address_components: [
        {
          long_name: 'France',
          short_name: 'FR',
          types: ['country', 'political'],
        },
      ],
      formatted_address: 'France',
      geometry: {
        bounds: {
          northeast: {
            lat: 51.1241999,
            lng: 9.6624999,
          },
          southwest: {
            lat: 41.3253001,
            lng: -5.5591,
          },
        },
        location: {
          lat: 46.227638,
          lng: 2.213749,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          northeast: {
            lat: 51.1241999,
            lng: 9.6624999,
          },
          southwest: {
            lat: 41.3253001,
            lng: -5.5591,
          },
        },
      },
      place_id: 'ChIJMVd4MymgVA0R99lHx5Y__Ws',
      types: ['country', 'political'],
    },
  ],
};
