import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { IUserSearchCriteria, IVenueCategoriesResponse, IVenueDetailsResponse, IVenueSearchResponse } from '../../../models';
import { IGeoCodeResponse } from '../../../models/index';
import { fakeGeoCodeResponse } from './fake-responses/fake-geo-code-responses';
import { fakeVenueDetails } from './fake-responses/fake-venue-details-response';
import { fakeVenuesCategories } from './fake-responses/fake-venues-categories';
import { fakeVenuesSearchResponse } from './fake-responses/fake-venues-search-response';

@Injectable()
export class MockDataProviderService {

  public convertPositionIntoAddress(latitude: number, longitude: number): Promise<IGeoCodeResponse> {
    return of(fakeGeoCodeResponse as any).toPromise();
  }

  public loadVenueDetails(venueId: string): Promise<IVenueDetailsResponse> {
    return of(fakeVenueDetails as any).toPromise();
  }

  public searchVenues(userSearchCriteria: IUserSearchCriteria): Promise<IVenueSearchResponse> {
    return of(fakeVenuesSearchResponse as any).toPromise();
  }

  public loadVenuesCategories(): Promise<IVenueCategoriesResponse> {
    return of(fakeVenuesCategories as any).toPromise();
  }
}
